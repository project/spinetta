<?php


/**
 * We recover a query with the content type: event
 *
 * @param $type
 * @param $page
 * @param $range
 **/
function _spinetta_events($type, $page, $range){
  $query = db_select('node','n');
  $query->innerJoin('field_data_body','b','b.entity_id = n.nid');
  $query->innerJoin('field_data_field_event_image','ei','ei.entity_id = n.nid');
  $query->leftJoin('field_data_field_event_date','ed','ed.entity_id = n.nid');
  $query->leftJoin('field_data_field_event_venue','ev','ev.entity_id = n.nid');
  $query->leftJoin('field_data_field_event_location','el','el.entity_id = n.nid');

  // query title and body
  $query = $query
    ->fields('n', array('title', 'nid'))
    ->fields('b', array('body_value'));

  if($type == 'gallery'){
    $query = $query->fields('ei', array('field_event_image_fid'));
  }
  
  if($type == 'events' || $type == 'upcoming'){
    $query = $query
      ->fields('ed', array('field_event_date_value'))
      ->fields('ev', array('field_event_venue_value'))
      ->fields('el', array('field_event_location_value'));
  }


  $query = $query->condition('n.type', 'event');

  if($type == 'upcoming'){
    $query = $query->condition('ed.field_event_date_value', REQUEST_TIME, '>=');
    $query = $query->orderBy('ed.field_event_date_value', 'ASC');
  } else {
    $query = $query->orderBy('n.created','DESC');
  }
  
  $query = $query
    ->range($page,$range)
    ->execute()
    ->fetchAll();
    
  return $query;
}


/**
 * Block retrieve articles all query
 **/
function _spinetta_articles($page, $range){
  $query = db_select('node','n');
  $query->innerJoin('field_data_body','b','b.entity_id = n.nid');
  $query->innerJoin('users','u','u.uid = n.uid');
  $query->innerJoin('field_data_field_tags','t','t.entity_id = n.nid');
  $query->innerJoin('taxonomy_term_data','ttd','ttd.tid = t.field_tags_tid');
  $query->leftJoin('field_data_field_image','fi','fi.entity_id = n.nid');
  $query->leftJoin('field_data_field_article_sound','fas','fas.entity_id = n.nid');
  $query->leftJoin('node_comment_statistics','ncs','ncs.nid = n.nid');

  $query = $query
    ->fields('n', array('title', 'nid', 'created', 'uid'))
    ->fields('u', array('name'))
    ->fields('b', array('body_value'))
    ->fields('ttd', array('name'))
    ->fields('fi', array('field_image_fid'))
    ->fields('fas', array('field_article_sound_value'))
    ->fields('ncs', array('comment_count'))
    ->condition('n.type','article')
    ->orderBy('n.created','DESC')
    ->range($page,$range)
    ->execute()
    ->fetchAll();
    
  return $query;
}