<ul class="tour-dates">
<?php foreach($vars as $item) { ?>
  <li class="einfo">
    <div class="post-date"><?php print $item['date'] ?></div>
    <div class="post-meta-more">
      <h3><a href="#"><?php print $item['title'] ?></a></h3>
      <p><?php print $item['location'] ?></p>
      <a href="#myModalticket" data-toggle="modal" class="btn"><i class="icon-basket"></i>Buy Tickets </a>
    </div>
  </li>
<?php } ?>
</ul>