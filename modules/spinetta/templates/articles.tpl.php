                <?php foreach ($vars as $key => $item) {?>
                  <?php 
                    if($key == 0 || $key == 2) {
                      print '<div class="row-fluid">';
                    }
                  ?>
                    <article class="post span6">
                        <?php print $item['image'] ?>
                        <?php print $item['sound'] ?>
                        <h3><a href="#"> <?php print $item['title'] ?> </a></h3>
                        <div class="post-meta">
                            <a href="#" title="Posts by author"><i class="icon-user"></i> <?php print $item['author'] ?></a>
                            <a href="#" title="Posts by author"><i class="icon-clock"></i> <?php print $item['date'] ?></a>
                            <a href="#" title="Posts by author"><i class="icon-doc-text"></i> <?php print $item['tag'] ?></a>
                            <a href="#" title="Posts by author"><i class="icon-comment"></i> <?php print $item['comment'] ?></a>
                        </div>
                        <p><?php print $item['body'] ?></p>
                    </article>
                  <?php
                    if($key == 1 || $key == 3) {
                      print '</div>';
                    }
                  ?>
                <?php } ?>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <a href="#" class="btn ctablog"> Read all the news at our Blog </a>
                    </div>
                </div>