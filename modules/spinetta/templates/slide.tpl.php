<ul class="slides">
  <?php if(variable_get('slide_title_1')) : ?>
  <li class="item-caption">
		    <h1><?php print variable_get('slide_title_1') ?></h1>
		    <h2><?php print variable_get('slide_desc_1') ?></h2>
	    </li>
  <?php endif; ?>
  
  <?php if(variable_get('slide_title_2')) : ?>
  <li class="item-caption">
    <h1><?php print variable_get('slide_title_2') ?></h1>
    <h2><?php print variable_get('slide_desc_2') ?></h2>
  </li>
  <?php endif; ?>

  <?php if(variable_get('slide_title_3')) : ?>
  <li class="item-caption">
    <h1><?php print variable_get('slide_title_3') ?></h1>
    <h2><?php print variable_get('slide_desc_3') ?></h2>
  </li>
  <?php endif; ?>

  <?php if(variable_get('slide_title_4')) : ?>
  <li class="item-caption">
    <h1><?php print variable_get('slide_title_4') ?></h1>
    <h2><?php print variable_get('slide_desc_4') ?></h2>
  </li>
  <?php endif; ?>
</ul> 
