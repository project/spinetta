<ul id="bx-pager">
  <?php if(variable_get('slide_logo')) : ?>
  <li class="span3">
    <a href="#" class="tooltip_hover" data-placement="bottom"><img src="<?php print base_path().'sites/default/files/block_image/'.variable_get('slide_logo') ?>" alt="logo" width="142" height="95"></a>
  </li>
  <?php endif ?>
  <?php if(variable_get('slide_logo1')) : ?>
  <li class="span3">
    <a href="#" class="tooltip_hover" data-placement="bottom"><img src="<?php print base_path().'sites/default/files/block_image/'.variable_get('slide_logo1') ?>" alt="logo" width="142" height="95"></a>
  </li>
  <?php endif ?>
  <?php if(variable_get('slide_logo2')) : ?>
  <li class="span3">
    <a href="#" class="tooltip_hover" data-placement="bottom"><img src="<?php print base_path().'sites/default/files/block_image/'.variable_get('slide_logo2') ?>" alt="logo" width="142" height="95"></a>
  </li>
  <?php endif ?>
  <?php if(variable_get('slide_logo3')) : ?>
  <li class="span3">
    <a href="#" class="tooltip_hover" data-placement="bottom"><img src="<?php print base_path().'sites/default/files/block_image/'.variable_get('slide_logo3') ?>" alt="logo" width="142" height="95"></a>
  </li>
  <?php endif ?>
</ul>
