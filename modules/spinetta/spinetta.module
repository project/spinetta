<?php

require_once 'spinetta.database.inc'; // queries to be reused in home and accross sections should be here


/**
 * Implements  hook_block_info().
 */
function spinetta_block_info(){

  $blocks['spinetta_gallery'] = array(
    'info' => 'Spinetta - Gallery',
  );

  $blocks['spinetta_events'] = array(
    'info' => 'Spinetta - Events',
  );

  $blocks['spinetta_events_upcoming'] = array(
    'info' => 'Spinetta - Upcoming',
  );

  $blocks['spinetta_articles'] = array(
    'info' => 'Spinetta - Articles',
  );

  $blocks['spinetta_slide'] = array(
    'info' => 'Spinetta - Slide',
  );

  $blocks['spinetta_logos'] = array(
    'info' => 'Spinetta - Logos',
  );

  return $blocks;
}

/**
 * Implements  hook_block_view().
 */
function spinetta_block_view($delta){

  $block = array();


  switch ($delta) {

    case 'spinetta_gallery':
      $type = 'gallery';
      $block['content'] = spinetta_gallery($type);
      break;

    case 'spinetta_events':
      $type = 'events';
      $block['content'] = spinetta_events($type);
      break;

    case 'spinetta_events_upcoming':
      $type = 'upcoming';
      $block['content'] = spinetta_events_upcoming($type);
      break;

    case 'spinetta_articles':
      $block['content'] = spinetta_articles();
      break;

    case 'spinetta_slide':
      $block['content'] = theme('slide');
      break;

    case 'spinetta_logos':
      $block['content'] = theme('logos');
      break;

  }

  return $block;
}

/**
 * Implements  hook)theme().
 */
function spinetta_theme(){

  return array(
    'articles' => array(
      'template' => 'templates/articles',
      'path' => drupal_get_path('module', 'spinetta'),
    ),
    'slide' => array(
      'template' => 'templates/slide',
      'path' => drupal_get_path('module', 'spinetta'),
    ),
    'events_upcoming' => array(
      'template' => 'templates/events-upcoming',
      'path' => drupal_get_path('module', 'spinetta'),
    ),
    'logos' => array(
      'template' => 'templates/logos',
      'path' => drupal_get_path('module', 'spinetta'),
    ),
  );
}


/**
 * We generate blocks with the image of each event to the block
 *
 * Block: Our Concert Gallery
 *
 * @param $type.
 *   The $ type parameter defined brings the content type that we want to show in the block.
 */
function spinetta_gallery($type){

  $results= array(0,3,6);

  $content = '';

  foreach($results as $page){
    $result = _spinetta_events($type, $page , 3);

    $content.='<div class="row-fluid">';
    foreach($result as $item){
      $image = file_load($item->field_event_image_fid);
      $image_url = file_create_url($image->uri);

      $content.= '<div class="span4">
                  <a href="'.$image_url.'" class="lightbox_image port-item">
                    <span class="hovers"> <i class="icon-plus-circled"></i> </span>
                    <img src="'.$image_url.'" alt="image">
                  </a>
                  <div class="imageinfo">
                    <h2 class="text-center"><a href="'.$image_url.'" class="lightbox_image"> '.$item->title.' </a></h2>
                    <p class="text-center">'.$item->body_value.'</p>
                  </div>
                 </div>';
    }
    $content.='</div>';
  }

  return $content;
}


/**
 * Generate an event list, retrieve the date, place of event, location fields.
 *
 * World Wide Tour Table
 *
 * @param $type.
 *   The $ type parameter defined brings the content type that we want to show in the block.
 */
function spinetta_events($type){
  $result = _spinetta_events($type, 0 , 8);
  $cols = array('date','venue','location','tickets');
  $content = '';

  $content.= '<div class="row-fluid">
        	<div class="span12">';
  foreach($cols as $col){
    $content.= '<ul class="pricing-table">
                  <li class="header"> <h2>'.$col.' </h2></li>';

    if($col == 'date'){
      foreach($result as $item){
        $date = date("d/m/Y",$item->field_event_date_value);
        $content.='<li><i class="icon-calendar"></i>'.$date.'</li>';
      }
    } elseif($col == 'venue'){
      foreach($result as $item){
        $content.='<li><i class="icon-video"></i>'.$item->field_event_venue_value.'</li>';
      }
    } elseif($col == 'location'){
      foreach($result as $item){
        $content.='<li><i class="icon-location"></i>'.$item->field_event_location_value.'</li>';
      }
    } else {
      foreach($result as $item){
        $content.='<li> <a href="#myModalticket" data-toggle="modal" class="activated"><i class="icon-basket"></i>Buy Tickets #'.$item->nid.'</a></li>';
      }
    }

    $content.='</ul>';
  }

  $content.='</div></div>';

  return $content;
}


/**
 * We recover the last 4 articles published
 *
 * Block: Latest News
 */
function spinetta_articles(){
  $result = _spinetta_articles(0 , 4);

  $vars = array();
  foreach($result as $key => $item){
    $vars[$key]['title']= $item->title;
    $vars[$key]['body']=$item->body_value;
    $vars[$key]['author']=$item->name;
    $date = date("M d Y",$item->created);
    $vars[$key]['date']=$date;
    $vars[$key]['tag']=$item->ttd_name;

    // load image
    if(!empty($item->field_image_fid)) {
      $image = file_load($item->field_image_fid);
      $vars[$key]['image']= image_style_url('teaser', $image->uri);
      $vars[$key]['image']= '<img src="'.$vars[$key]['image'].'" alt="image" />';
    } else {
      $vars[$key]['image']= '';
    }
    $vars[$key]['sound']=$item->field_article_sound_value;
    $vars[$key]['comment']=$item->comment_count.'   Comments';
  }


  return theme('articles', array('vars' => $vars));
}

/**
 *  We retrieve the next event
 *
 * Block: Upcoming Event
 *
 * @param $type.
 *   The $ type parameter defined brings the content type that we want to show in the block.
 */
function spinetta_events_upcoming($type){
  $result = _spinetta_events($type, 0 , 3);
  $vars = array();

  foreach($result as $key => $item){
    $vars[$key]['title']= $item->title;
    $date = date("d  M",$item->field_event_date_value);
    $vars[$key]['date']=$date;
    $vars[$key]['title']= $item->title;
    $vars[$key]['location']= $item->field_event_location_value;
  }
  return theme('events_upcoming', array('vars' => $vars));
}


/**
 * Implements hook_block_configure().
 */
function spinetta_block_configure($delta='') {
  $form = array();

  switch($delta) {
    case 'spinetta_slide' :

      $form['slide_1'] = array(
      '#type' => 'fieldset',
      '#title' => t('Slide'),
      '#weight' => 0,
      '#collapsible' => TRUE,
      );

      $form['slide_1']['slide_title_1'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Title'),
        '#default_value' => variable_get('slide_title_1', ''),
      );
      $form['slide_1']['slide_desc_1'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Description'),
        '#default_value' => variable_get('slide_desc_1', ''),
      );

      $form['slide_2'] = array(
      '#type' => 'fieldset',
      '#title' => t('Slide 2'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      );
      $form['slide_2']['slide_title_2'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Title'),
        '#default_value' => variable_get('slide_title_2', ''),
      );
      $form['slide_2']['slide_desc_2'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Description'),
        '#default_value' => variable_get('slide_desc_2', ''),
      );

      $form['slide_3'] = array(
      '#type' => 'fieldset',
      '#title' => t('Slide 3'),
      '#weight' => 2,
      '#collapsible' => TRUE,
      );
      $form['slide_3']['slide_title_3'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Title'),
        '#default_value' => variable_get('slide_title_3', ''),
      );
      $form['slide_3']['slide_desc_3'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Description'),
        '#default_value' => variable_get('slide_desc_3', ''),
      );

      $form['slide_4'] = array(
      '#type' => 'fieldset',
      '#title' => t('Slide 4'),
      '#weight' => 3,
      '#collapsible' => TRUE,
      );
      $form['slide_4']['slide_title_4'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Title'),
        '#default_value' => variable_get('slide_title_4', ''),
      );
      $form['slide_4']['slide_desc_4'] = array(
        '#type' => 'textfield',
        '#title' => t('Event Description'),
        '#default_value' => variable_get('slide_desc_4', ''),
      );
      break;

    case 'spinetta_logos' :

      $form['slide_logo'] = array(
        '#name' => 'logo',
        '#type' => 'managed_file',
        '#title' => t('Choose an Image File'),
        '#description' => t('Select an Image for the custom block.  Only *.gif, *.png, *.jpg, and *.jpeg images allowed.'),
        '#default_value' => variable_get('slide_logo', ''),
        '#upload_location' => 'public://block_image/',
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
      );

      $form['slide_logo1'] = array(
        '#name' => 'logo1',
        '#type' => 'managed_file',
        '#title' => t('Choose an Image File'),
        '#description' => t('Select an Image for the custom block.  Only *.gif, *.png, *.jpg, and *.jpeg images allowed.'),
        '#default_value' => variable_get('slide_logo1', ''),
        '#upload_location' => 'public://block_image/',
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
      );

      $form['slide_logo2'] = array(
        '#name' => 'logo2',
        '#type' => 'managed_file',
        '#title' => t('Choose an Image File'),
        '#description' => t('Select an Image for the custom block.  Only *.gif, *.png, *.jpg, and *.jpeg images allowed.'),
        '#default_value' => variable_get('slide_logo1', ''),
        '#upload_location' => 'public://block_image/',
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
      );

      $form['slide_logo3'] = array(
        '#name' => 'logo3',
        '#type' => 'managed_file',
        '#title' => t('Choose an Image File'),
        '#description' => t('Select an Image for the custom block.  Only *.gif, *.png, *.jpg, and *.jpeg images allowed.'),
        '#default_value' => variable_get('slide_logo1', ''),
        '#upload_location' => 'public://block_image/',
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
      );

      break;
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function spinetta_block_save($delta = '', $edit = array()) {
  switch($delta) {
    case 'spinetta_slide' :
      // Variables slides save
      variable_set('slide_title_1', $edit['slide_title_1']);
      variable_set('slide_desc_1', $edit['slide_desc_1']);
      variable_set('slide_title_2', $edit['slide_title_2']);
      variable_set('slide_desc_2', $edit['slide_desc_2']);
      variable_set('slide_title_3', $edit['slide_title_3']);
      variable_set('slide_desc_3', $edit['slide_desc_3']);
      variable_set('slide_title_4', $edit['slide_title_4']);
      variable_set('slide_desc_4', $edit['slide_desc_4']);
      break;

    case 'spinetta_logos':
      //
      $logo = file_load($edit['slide_logo']);
      variable_set('slide_logo', $logo->filename);

      $logo1 = file_load($edit['slide_logo1']);
      variable_set('slide_logo1', $logo1->filename);

      $logo2 = file_load($edit['slide_logo2']);
      variable_set('slide_logo2', $logo2->filename);

      $logo3 = file_load($edit['slide_logo3']);
      variable_set('slide_logo3', $logo3->filename);

  }
}
