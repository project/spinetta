<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div id="page">

  <!-- Main Menu -->
  <nav class="navbar navbar-fixed-top animated fadeInDown delay2">
    <div class="navbar-inner">
      <div class="container">
        <!--Mobile Main Menu-->
        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!--Mobile Main Menu-->

        <!-- Brand-->
        <img src="<?php print base_path().path_to_theme(); ?>/img/logo.png" alt="logo" class="brand">
        <!-- End Brand-->

        <!--Desktop Main Menu-->
        <div class="nav-collapse collapse">
          <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('nav pull-right')))); ?>
        </div>
        <!--End Desktop Main Menu-->

      </div><!--/.container -->
    </div><!--/.nav-inner -->
  </nav>
  <!-- End Main Menu -->


  <!-- Contact Modal -->
  <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelc" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabelc">Contact Us!</h3>
    </div>
    <div class="modal-body">
        <!-- Form -->
        <form id="form" class="form_online" action="send_mail.php">
          <label class="no">Name</label>
          <input type="text" placeholder="Name" name="name">
          <label class="no">Email</label>
          <input type="email" placeholder="Email" name="email" required>
          <label class="no">Message</label>
          <textarea placeholder="Your Message" name="message" required></textarea>
          <input type="submit" name="Submit" value="Send" class="send">
        </form>
        <!-- Form -->
    </div>
    <div class="modal-footer"></div>
  </div>



  <!-- Header Section -->
  <header class="parallax-bg1 parallax clearfix" id="menu-home">
    <div class="container">
      <div class="row-fluid flexheader">
        <?php print render($page['slide']); ?>
      </div>
    </div>
  </header>
  <!-- End Header Section -->


  <!-- Band Members Section -->
  <section class="band generic container" id="menu-band">
    <!-- Title -->
    <div class="row-fluid text-center title">
        <h1>About <small>Band</small> Members</h1>
    </div>
    <!-- End Title -->

    <?php
      $block = module_invoke('block', 'block_view', 2);
      print $block['content'];
    ?>
  </section>
  <!-- End Band Members Section -->


  <!-- Logos Section -->
  <section class="parallax-bg2 parallax clearfix logos">
  	<div class="container">
      <div class="row-fluid">
        <?php print render($page['logos']); ?>
      </div>
    </div>
  </section>
  <!-- End Logos Section -->


  <!-- Gallery Section -->
  <section class="gallery generic container" id="menu-gallery">
    <!-- Title -->
    <div class="row-fluid text-center title">
        <h1>Our <small>Concert</small> gallery </h1>
    </div>
    <!-- End Title -->

    <?php print render($page['gallery']); ?>
  </section>
  <!-- End Gallery Section -->


	<section class="tour generic container" id="menu-tour">

    <!-- Title -->
    <div class="row-fluid text-center title">
        <h1>WORLDWIDE <small>Tour</small> Table</h1>
    </div>
    <!-- End Title -->

    <?php print render($page['events']); ?>

  </section>



  <!-- Ticket Modal -->
  <div id="myModalticket" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Buy Ticket Now</h3>
    </div>
    <div class="modal-body text-center">
        <p>Buy the theme now and get free support and free update!</p>
        <img src="<?php print base_path().path_to_theme(); ?>/img/payment.png" alt="payment">
    </div>
    <div class="modal-footer"></div>
  </div>



	<!-- Video / Accordion Section -->
	<section class="parallax-bg3 parallax clearfix extra">
     <?php
        $block = module_invoke('block', 'block_view', 6);
        print $block['content'];
     ?>
  </section>
	<!-- End Video / Accordion Section -->



  <!-- Discography Section -->
  <section class="discography generic container" id="menu-discography">

    <!-- Title -->
    <div class="row-fluid text-center title">
        <h1>Enjoy <small>the band</small> videos </h1>
    </div>
    <!-- End Title -->

    <!-- Accordion -->
    <?php
      $block = module_invoke('block', 'block_view', 7);
        print $block['content'];
    ?>
    <!-- End Accordion -->
  </section>
  <!-- Discography Section -->




	<!-- Videos Section -->
	<section class="videos generic container" id="menu-videos">
    <!-- Title -->
    <div class="row-fluid text-center title">
        <h1>Enjoy <small>the band</small> videos </h1>
    </div>
    <!-- End Title -->


    <div class="row-fluid">
        <div id="myCarousel" class="carousel slide span12">
          <!-- Carousel items -->
          <div class="carousel-inner">

            <div class="active item row-fluid">
                <?php print render($page['videos']); ?>
            </div>


          </div>
          <!-- Carousel nav -->
          <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
          <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
    </div>
  </section>
	<!-- End Videos Section -->


	<!-- twitter Section -->
	<section class="parallax-bg4 parallax clearfix twitter">
  	<div class="container">
    	<div class="row-fluid text-center">
        <i class="icon-twitter icon-2x"></i>
        <h3 class="username"></h3>
        <p class="tweet twitterfeed">
        </p>
        <p class="date"></p>
      </div>
    </div>
  </section>
	<!-- End twitter Section -->


  <!-- News Section -->
  <section class="blog generic container" id="menu-blog">
    <!-- Title -->
    <div class="row-fluid text-center title">
        <h1>Blog <small> stay up to </small> date</h1>
    </div>
    <!-- End Title -->

    <div class="row-fluid">
      <div class="span9">
        <h2>Latest News</h2>

        <?php print render($page['articles']); ?>

      </div>

      <div class="span3">
        <h2>Upcoming events</h2>

        <?php print render($page['upcoming']); ?>


        <?php
          $block = module_invoke('block', 'block_view', 10);
          print $block['subject'];
          print $block['content'];
        ?>
      </div>
    </div>

  </section>
  <!-- End News Section -->



  <div id="map">
    <?php
      $block = module_invoke('block', 'block_view', 3);
      print $block['content'];
    ?>
  </div>


  <footer>
  	<div class="container">
        <?php
          $block = module_invoke('block', 'block_view', 4);
          print $block['content'];
        ?>
      </div>
  </footer>


  <a href="#" class="scrollup"><i class="icon-up-open"></i></a>
</div>

<?php print render($page['bottom']); ?>
