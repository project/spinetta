<?php
/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function spinetta_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL)  {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  /*if (isset($form_id)) {
    return;
  }*/

  $css_color = array(
        'blue' => 'Blue',
        'brown' => 'Brown',
        'cream' => 'Cream',
        'cyan' => 'Cyan',
        'green' => 'Green',
        'lemon' => 'Lemon',
        'orange' => 'Orange',
        'pink' => 'Pink',
        'red' => 'Red',
        'violet' => 'Violet',
        'wine' => 'Wine',
        'yellow' => 'Yellow',
    );

    $form['spinetta'] = array(
        '#type' => 'fieldset',
        '#title' => t('Spinetta settings')
    );

    $form['elegantica']['spinetta_color'] = array(
        '#type' => 'select',
        '#options' => $css_color,
        '#title' => t('Theme color style'),
        '#default_value' => theme_get_setting('spinetta_color', 'spinetta'),
        '#description' => t('Change theme color style'),
    );

    return $form;

}
