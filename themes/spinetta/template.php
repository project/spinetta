<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  STARTERKIT_preprocess_html($variables, $hook);
  STARTERKIT_preprocess_page($variables, $hook);
}
// */


/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */

function spinetta_preprocess_html(&$variables) {
  drupal_add_js(base_path() . path_to_theme() .'/js/jquery.1.8.2.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/bootstrap/bootstrap.min.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/parallax.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/jquery.flexslider-min.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/twitter/jquery.tweet.min.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/nav/jquery.scrollTo.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/nav/jquery.nav.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/retina/retina.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/audioplayer.min.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/fancybox/jquery.fancybox.pack.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/jquery.fitvids.min.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/jquery.placeholder.min.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_js(base_path() . path_to_theme() .'/js/jquery-func.js', array('type' => 'file', 'scope' => 'footer'));

  $path = path_to_theme();

  $color_setting_style = theme_get_setting('spinetta_color', 'spinetta');
    if (file_exists($path . '/css/color/' . $color_setting_style . '.css')) {
       drupal_add_css($path . '/css/color/' . $color_setting_style . '.css');
    }
    /** color change */
    if (!empty($_GET['color']) || !empty($_SESSION['css_color'])) {
      $color = isset($_GET['color']) ? $_GET['color'] : $_SESSION['css_color'];
      $_SESSION['css_color'] = $color;
      if (file_exists($path . '/css/color/' . $color . '.css')) {
          drupal_add_css($path . '/css/color/' . $color . '.css');
      }
    }
}


function spinetta_menu_item_link($item, $link_item) {
  // Convert anchors in path to proper fragment
  $path = explode('#', $link_item['path'], 2);
  $fragment = !empty($path[1]) ? $path[1] : NULL;
  $path = $path[0];
  return l(
    $item['title'],
    $path,
    !empty($item['description']) ? array('title' => $item['description']) : array(),
    !empty($item['query']) ? $item['query'] : NULL,
    $fragment,
    FALSE,
    FALSE
  );
}